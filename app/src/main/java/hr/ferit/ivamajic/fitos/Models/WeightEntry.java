package hr.ferit.ivamajic.fitos.Models;

public class WeightEntry {
    private String id, date;
    private double weight;


    public WeightEntry(String id, String date, double weight) {
        this.id = id;
        this.date = date;
        this.weight = weight;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
