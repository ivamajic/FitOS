package hr.ferit.ivamajic.fitos.Models;

public class Macros {
    private int kcal, carbs, protein, fats;

    public Macros(int kcal, int carbs, int protein, int fats) {
        this.kcal = kcal;
        this.carbs = carbs;
        this.protein = protein;
        this.fats = fats;
    }

    public int getKcal() {
        return kcal;
    }

    public void setKcal(int kcal) {
        this.kcal = kcal;
    }

    public int getCarbs() {
        return carbs;
    }

    public void setCarbs(int carbs) {
        this.carbs = carbs;
    }

    public int getProtein() {
        return protein;
    }

    public void setProtein(int protein) {
        this.protein = protein;
    }

    public int getFats() {
        return fats;
    }

    public void setFats(int fats) {
        this.fats = fats;
    }
}
