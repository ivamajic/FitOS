package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.ivamajic.fitos.Models.WeightEntry;
import hr.ferit.ivamajic.fitos.R;

public class AddWeightHeightActivity extends AppCompatActivity {

    @BindView(R.id.etWeightCurrentWeight) EditText etCurrentWeight;
    @BindView(R.id.etWeightAge) EditText etAge;
    @BindView(R.id.etWeightHeight) EditText etHeight;
    @BindView(R.id.etWeightGoalWeight) EditText etGoalWeight;
    @BindView(R.id.btnWeightSubmit) Button btnSubmit;
    @BindView(R.id.spinnerWeightActivity) Spinner spinnerActivity;
    @BindView(R.id.rgWeightGender) RadioGroup rgGender;

    private ArrayList<String> activityLevels = new ArrayList<>();

    private String selectedGender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_weight_height);

        ButterKnife.bind(this);

        activityLevels.add("Sedentary");
        activityLevels.add("Lightly active");
        activityLevels.add("Moderately active");
        activityLevels.add("Very active");
        activityLevels.add("Extremely active");
        ArrayAdapter<String> spinnerAdapterGoals = new ArrayAdapter<>(this, R.layout.spinner_layout, activityLevels);
        spinnerAdapterGoals.setDropDownViewResource(R.layout.spinner_layout);
        spinnerActivity.setAdapter(spinnerAdapterGoals);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }

    @OnClick({R.id.btnWeightSubmit})
    public void addMeasurements() {
        if (etCurrentWeight.getText().toString().equals("") || etAge.getText().toString().equals("") || etGoalWeight.getText().toString().equals("") || etHeight.getText().toString().equals("") || rgGender.getCheckedRadioButtonId() == -1) {
            Toast.makeText(this, "Please fill out all the fields!", Toast.LENGTH_SHORT).show();
        }
        else {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            DatabaseReference database = FirebaseDatabase.getInstance().getReference("users/" + user.getUid() + "/height");
            database.setValue(etHeight.getText().toString());
            database = FirebaseDatabase.getInstance().getReference("users/" + user.getUid() + "/weightGoal");
            database.setValue(etGoalWeight.getText().toString());
            database = FirebaseDatabase.getInstance().getReference("users/" + user.getUid() + "/age");
            database.setValue(etAge.getText().toString());

            int selectedRBId = rgGender.getCheckedRadioButtonId();
            RadioButton selectedButton = (RadioButton) findViewById(selectedRBId);
            String rbText = selectedButton.getText().toString();
            database = FirebaseDatabase.getInstance().getReference("users/" + user.getUid() + "/gender");
            database.setValue(rbText);

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();


            database = FirebaseDatabase.getInstance().getReference("users/" + user.getUid() + "/weight");
            database.setValue(etCurrentWeight.getText().toString());
            WeightEntry startWeight = new WeightEntry(null, dateFormat.format(date), Double.parseDouble(etCurrentWeight.getText().toString()));
            database = FirebaseDatabase.getInstance().getReference("users/" + user.getUid() + "/weightEntries");
            database.setValue(startWeight);



            Intent action = new Intent(this, AddGoalsActivity.class);
            action.putExtra("activityLevel", spinnerActivity.getSelectedItem().toString());
            startActivity(action);
        }

    }
}
