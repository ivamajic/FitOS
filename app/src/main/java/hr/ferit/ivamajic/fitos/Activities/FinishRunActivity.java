package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.ivamajic.fitos.Models.RunEntry;
import hr.ferit.ivamajic.fitos.R;

public class FinishRunActivity extends AppCompatActivity {

    @BindView(R.id.tvFinishRunAvgSpeed) TextView tvAvgSpeed;
    @BindView(R.id.tvFinishRunDistance) TextView tvDistance;
    @BindView(R.id.tvFinishRunDuration) TextView tvDuration;
    @BindView(R.id.tvFinishRunMaxSpeed) TextView tvMaxSpeed;
    @BindView(R.id.btnFinishRunSave) Button btnSave;

    RunEntry runEntry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_run);

        ButterKnife.bind(this);

        Bundle bundle = this.getIntent().getExtras();

        String run = bundle != null ? bundle.getString("runEntry") : null;
        runEntry = new Gson().fromJson(run, RunEntry.class);

        Log.d("test", runEntry.getDate());
        Log.d("test", String.valueOf(runEntry.getAvgSpeed()));
        Log.d("test", String.valueOf(runEntry.getDistance()));

        tvAvgSpeed.setText(String.format("%.2f", runEntry.getAvgSpeed()));
        tvDistance.setText(String.format("%.2f", runEntry.getDistance()));
        tvDuration.setText(String.format("%.2f", runEntry.getDuration()));
        tvMaxSpeed.setText(String.format("%.2f", runEntry.getMaxSpeed()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }

    @OnClick({R.id.btnFinishRunSave})
    public void saveRun() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user==null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }

        DatabaseReference database = FirebaseDatabase.getInstance().getReference("runEntries/" + user.getUid());
        database.push().setValue(runEntry);
        Toast.makeText(this, "Run successfully added", Toast.LENGTH_SHORT).show();
        finish();
    }
}
