package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.ivamajic.fitos.Adapters.ExerciseAdapter;
import hr.ferit.ivamajic.fitos.Models.Exercise;
import hr.ferit.ivamajic.fitos.Models.ExerciseEntry;
import hr.ferit.ivamajic.fitos.R;

public class ExerciseActivity extends AppCompatActivity {

    @BindView(R.id.btnExerciseLogExercise) Button btnLogExercise;
    @BindView(R.id.tvExercisePrevious) TextView tvPrevious;
    @BindView(R.id.tvExerciseNext) TextView tvNext;
    @BindView(R.id.tvExerciseDate) TextView tvDate;
    ListView lvExercises;

    Calendar currentDate, today, yesterday, tomorrow;

    SimpleDateFormat sdf;

    ArrayList<ExerciseEntry> exerciseEntries = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);

        ButterKnife.bind(this);

        sdf = new SimpleDateFormat(getString(R.string.dateFormat));

        currentDate = Calendar.getInstance();
        tvDate.setText(R.string.today);


        today = Calendar.getInstance();
        int todayDate = today.get(Calendar.DAY_OF_MONTH);

        yesterday = Calendar.getInstance();
        yesterday.set(Calendar.DAY_OF_MONTH, todayDate - 1);

        tomorrow = Calendar.getInstance();
        tomorrow.set(Calendar.DAY_OF_MONTH, todayDate + 1);


        searchForExercises();
    }


    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }

    @OnClick({R.id.btnExerciseLogExercise})
    public void logExercise() {
        Intent action = new Intent(this, LogExerciseActivity.class);
        Log.d("test", String.valueOf(currentDate));
        action.putExtra("date", sdf.format(currentDate.getTime()));
        startActivity(action);
    }

    @OnClick({R.id.tvExerciseNext, R.id.tvExercisePrevious})
    public void changeDate(TextView textView) {
        int dayOfTheMonth = currentDate.get(Calendar.DAY_OF_MONTH);

        if (textView.getId() == R.id.tvExercisePrevious) {
            currentDate.set(Calendar.DAY_OF_MONTH, dayOfTheMonth - 1);
        }
        else {
            currentDate.set(Calendar.DAY_OF_MONTH, dayOfTheMonth + 1);
        }

        if (currentDate.getTime().toString().equals(today.getTime().toString())) {
            tvDate.setText(R.string.today);
        }
        else if (currentDate.getTime().toString().equals(tomorrow.getTime().toString())) {
            tvDate.setText(R.string.tomorrow);
        }
        else if (currentDate.getTime().toString().equals(yesterday.getTime().toString())) {
            tvDate.setText(R.string.yesterday);
        }
        else {
            tvDate.setText(sdf.format(currentDate.getTime()));
        }
        searchForExercises();
    }

    public void searchForExercises() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        else {
            DatabaseReference database = FirebaseDatabase.getInstance().getReference("exerciseEntries/" + user.getUid());
            database.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    exerciseEntries = new ArrayList<>();
                    for (DataSnapshot exerciseEntrySnapshot : dataSnapshot.getChildren()) {
                        if (exerciseEntrySnapshot.child("date").getValue().toString().equals(sdf.format(currentDate.getTime()))) {
                            int series = Integer.parseInt(exerciseEntrySnapshot.child("exercise").child("seriesNo").getValue().toString());
                            int repetitions = Integer.parseInt(exerciseEntrySnapshot.child("exercise").child("repetitions").getValue().toString());
                            int weight = Integer.parseInt(exerciseEntrySnapshot.child("exercise").child("weight").getValue().toString());

                            String name = exerciseEntrySnapshot.child("exercise").child("name").getValue().toString();

                            Exercise exercise = new Exercise(name, repetitions, weight, series);
                            ExerciseEntry exerciseEntry = new ExerciseEntry(exerciseEntrySnapshot.getKey(), exerciseEntrySnapshot.child("date").getValue().toString(), exercise);
                            exerciseEntries.add(exerciseEntry);
                        }
                    }

                    lvExercises = (ListView) findViewById(R.id.lvExerciseExercises);
                    ExerciseAdapter exerciseAdapter = new ExerciseAdapter(exerciseEntries);
                    lvExercises.setAdapter(exerciseAdapter);

                    lvExercises.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Intent action = new Intent(ExerciseActivity.this, ExerciseChartActivity.class);
                            action.putExtra("exerciseName", new Gson().toJson(exerciseEntries.get(i).getExercise().getName()));
                            startActivity(action);
                        }
                    });

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }
}
