package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.ivamajic.fitos.Models.Macros;
import hr.ferit.ivamajic.fitos.R;

public class AddGoalsActivity extends AppCompatActivity {

    @BindView(R.id.spinnerGoalsGoals) Spinner spinnerGoals;
    @BindView(R.id.spinnerGoalsMacros) Spinner spinnerMacros;
    @BindView(R.id.etGoalsKcal) EditText etKcal;
    @BindView(R.id.etGoalsCarbs) EditText etCarbs;
    @BindView(R.id.etGoalsProtein) EditText etProtein;
    @BindView(R.id.etGoalsFats) EditText etFats;
    @BindView(R.id.btnGoalsSubmit) Button btnSubmit;

    private ArrayList<String> goals = new ArrayList<>();
    private ArrayList<String> macros = new ArrayList<>();

    private String activityLevel = null;

    private int TDEE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_goals);

        ButterKnife.bind(this);

        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null) {
            activityLevel = bundle.getString("activityLevel");
        }

        /* Goals spinner initialization */
        goals.add("Lose weight moderately");
        goals.add("Lose weight fast");
        goals.add("Maintain weight");
        goals.add("Gain weight moderately");
        goals.add("Gain weight fast");
        ArrayAdapter<String> spinnerAdapterGoals = new ArrayAdapter<>(this, R.layout.spinner_layout, goals);
        spinnerAdapterGoals.setDropDownViewResource(R.layout.spinner_layout);
        spinnerGoals.setAdapter(spinnerAdapterGoals);
        spinnerGoals.setSelection(2);

        /* Macros spinner initialization */
        macros.add("Low carb/moderate protein/high fat");
        macros.add("Low carb/high protein/high fat");
        macros.add("Moderate carb/moderate protein/moderate fat");
        macros.add("Moderate carb/high protein/moderate fat");
        macros.add("High carb/moderate protein/low fat");
        macros.add("High carb/high protein/low fat");
        ArrayAdapter<String> spinnerAdapterMacros = new ArrayAdapter<>(this, R.layout.spinner_layout, macros);
        spinnerAdapterMacros.setDropDownViewResource(R.layout.spinner_layout);
        spinnerMacros.setAdapter(spinnerAdapterMacros);
        spinnerMacros.setSelection(0);


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference database = FirebaseDatabase.getInstance().getReference("users/" + user.getUid());
        database.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                TDEE = calculateTDEE(dataSnapshot.child("gender").getValue().toString(), Double.parseDouble(dataSnapshot.child("height").getValue().toString()), Double.parseDouble(dataSnapshot.child("weight").getValue().toString()), activityLevel, Integer.parseInt(dataSnapshot.child("age").getValue().toString()));
                Macros macros = calculateMacros(TDEE);
                etKcal.setText(String.valueOf(macros.getKcal()));
                etCarbs.setText(String.valueOf(macros.getCarbs()));
                etProtein.setText(String.valueOf(macros.getProtein()));
                etFats.setText(String.valueOf(macros.getFats()));
                spinnerMacros.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        int calorieGoals = TDEE;
                        switch (spinnerGoals.getSelectedItem().toString()) {
                            case "Lose weight moderately":
                                calorieGoals -= 500;
                                break;
                            case "Lose weight fast":
                                calorieGoals -= 1000;
                                break;
                            case "Maintain weight":
                                break;
                            case "Gain weight moderately":
                                calorieGoals += 500;
                                break;
                            case "Gain weight fast":
                                calorieGoals += 1000;
                                break;
                        }
                        Macros macros = calculateMacros(calorieGoals);
                        etKcal.setText(String.valueOf(macros.getKcal()));
                        etCarbs.setText(String.valueOf(macros.getCarbs()));
                        etProtein.setText(String.valueOf(macros.getProtein()));
                        etFats.setText(String.valueOf(macros.getFats()));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                spinnerGoals.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        int calorieGoals = TDEE;
                        switch (spinnerGoals.getSelectedItem().toString()) {
                            case "Lose weight moderately":
                                calorieGoals -= 500;
                                break;
                            case "Lose weight fast":
                                calorieGoals -= 1000;
                                break;
                            case "Maintain weight":
                                break;
                            case "Gain weight moderately":
                                calorieGoals += 500;
                                break;
                            case "Gain weight fast":
                                calorieGoals += 1000;
                                break;
                        }
                        Macros macros = calculateMacros(calorieGoals);
                        etKcal.setText(String.valueOf(macros.getKcal()));
                        etCarbs.setText(String.valueOf(macros.getCarbs()));
                        etProtein.setText(String.valueOf(macros.getProtein()));
                        etFats.setText(String.valueOf(macros.getFats()));                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                etKcal.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (etKcal.getTag() == null) {
                            int kcal;
                            if (editable.toString().equals("")) {
                                kcal = 0;
                            }
                            else {
                                kcal = Integer.parseInt(editable.toString());
                            }
                            Macros macros = calculateMacros(kcal);
                            etCarbs.setTag("x");
                            etFats.setTag("x");
                            etProtein.setTag("x");
                            etCarbs.setText(String.valueOf(macros.getCarbs()));
                            etProtein.setText(String.valueOf(macros.getProtein()));
                            etFats.setText(String.valueOf(macros.getFats()));
                            etCarbs.setTag(null);
                            etFats.setTag(null);
                            etProtein.setTag(null);
                        }
                    }
                });

                etFats.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (etFats.getTag() == null) {
                            int fats;
                            if (editable.toString().equals("")) {
                                fats = 0;
                            }
                            else {
                                fats = Integer.parseInt(editable.toString());
                            }
                            int totalKcal = fats * 9 + (Integer.parseInt(etCarbs.getText().toString()) + Integer.parseInt(etProtein.getText().toString())) * 4;
                            etKcal.setTag("x");
                            etKcal.setText(String.valueOf(totalKcal));
                            etKcal.setTag(null);
                        }
                    }
                });

                etCarbs.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (etCarbs.getTag() == null) {
                            int carbs;
                            if (editable.toString().equals("")) {
                                carbs = 0;
                            }
                            else {
                                carbs = Integer.parseInt(editable.toString());
                            }
                            int totalKcal = Integer.parseInt(etFats.getText().toString()) * 9 + (carbs + Integer.parseInt(etProtein.getText().toString())) * 4;
                            etKcal.setTag("x");
                            etKcal.setText(String.valueOf(totalKcal));
                            etKcal.setTag(null);
                        }

                    }
                });

                etProtein.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (etProtein.getTag() == null) {
                            int protein;
                            if (editable.toString().equals("")) {
                                protein = 0;
                            }
                            else {
                                protein = Integer.parseInt(editable.toString());
                            }
                            int totalKcal = Integer.parseInt(etFats.getText().toString()) * 9 + (Integer.parseInt(etCarbs.getText().toString()) + protein) * 4;
                            etKcal.setTag("x");
                            etKcal.setText(String.valueOf(totalKcal));
                            etKcal.setTag(null);
                        }

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }

    @OnClick({R.id.btnGoalsSubmit})
    public void submitGoals() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference database = FirebaseDatabase.getInstance().getReference("users/" + user.getUid() + "/calorieGoal");
        database.setValue(etKcal.getText().toString());
        database = FirebaseDatabase.getInstance().getReference("users/" + user.getUid() + "/proteinGoal");
        database.setValue(etProtein.getText().toString());
        database = FirebaseDatabase.getInstance().getReference("users/" + user.getUid() + "/carbGoal");
        database.setValue(etCarbs.getText().toString());
        database = FirebaseDatabase.getInstance().getReference("users/" + user.getUid() + "/fatGoal");
        database.setValue(etFats.getText().toString());

        Intent action = new Intent(this, MainActivity.class);
        action.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(action);
    }



    public int calculateTDEE(String gnd, Double height, Double weight, String activity, int age) {
        int BMR = 0;
        if (gnd.equals("F")) {
            BMR = (int) ((height * 625) + (weight * 9.99) - (age * 4.92) - 161);
        }
        else if (gnd.equals("M")) {
            BMR = (int) ((height * 625) + (weight * 9.99) - (age * 4.92) + 5);
        }
        double activityLevel = 0;
        switch (activity) {
            case "Sedentary":
                activityLevel = 1.2;
                break;
            case "Lightly active":
                activityLevel = 1.375;
                break;
            case "Moderately active":
                activityLevel = 1.55;
                break;
            case "Very active":
                activityLevel = 1.725;
                break;
            case "Extremely active":
                activityLevel = 1.9;
                break;
        }
        return (int) (BMR * activityLevel);
    }

    public Macros calculateMacros(int calorieGoals) {
        int proteins = 0, carbs = 0, fats = 0, calories = 0;
        switch (spinnerMacros.getSelectedItem().toString()) {
            case "Low carb/high protein/high fat":
                proteins = (int) (calorieGoals * 0.35 / 4);
                carbs = (int) (calorieGoals * 0.05 / 4);
                fats = (calorieGoals - (carbs + proteins) * 4) / 9;
                calories = (proteins + carbs) * 4 + fats * 9;
                break;
            case "Moderate carb/moderate protein/moderate fat":
                proteins = (int) (calorieGoals * 0.2 / 4);
                carbs = (int) (calorieGoals * 0.4 / 4);
                fats = (calorieGoals - (carbs + proteins) * 4) / 9;
                calories = (proteins + carbs) * 4 + fats * 9;
                break;
            case "Moderate carb/high protein/moderate fat":
                proteins = (int) (calorieGoals * 0.35 / 4);
                carbs = (int) (calorieGoals * 0.4 / 4);
                fats = (calorieGoals - (carbs + proteins) * 4) / 9;
                calories = (proteins + carbs) * 4 + fats * 9;
                break;
            case "High carb/moderate protein/low fat":
                proteins = (int) (calorieGoals * 0.2 / 4);
                carbs = (int) (calorieGoals * 0.5 / 4);
                fats = (calorieGoals - (carbs + proteins) * 4) / 9;
                calories = (proteins + carbs) * 4 + fats * 9;
                break;
            case "High carb/high protein/low fat":
                proteins = (int) (calorieGoals * 0.35 / 4);
                carbs = (int) (calorieGoals * 0.5 / 4);
                fats = (calorieGoals - (carbs + proteins) * 4) / 9;
                calories = (proteins + carbs) * 4 + fats * 9;
                break;
            default:
                proteins = (int) (calorieGoals * 0.2 / 4);
                carbs = (int) (calorieGoals * 0.05 / 4);
                fats = (calorieGoals - (carbs + proteins) * 4) / 9;
                calories = (proteins + carbs) * 4 + fats * 9;
                break;
        }
        return new Macros(calories, carbs, proteins, fats);
    }
}
