package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.ivamajic.fitos.Adapters.RunAdapter;
import hr.ferit.ivamajic.fitos.Models.Location;
import hr.ferit.ivamajic.fitos.Models.RunEntry;
import hr.ferit.ivamajic.fitos.R;

public class RunActivity extends AppCompatActivity {

    @BindView(R.id.btnRunStartARun) Button btnStartARun;
    ListView lvRuns;
    RunAdapter runAdapter;
    ArrayList<RunEntry> runEntries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run);

        ButterKnife.bind(this);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }


        DatabaseReference database = FirebaseDatabase.getInstance().getReference("runEntries/" + user.getUid());
        database.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                runEntries = new ArrayList<>();
                for(DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    ArrayList routes = new ArrayList();
                    for(DataSnapshot locSnapshot: snapshot.child("route").getChildren()) {
                        double latitude = Double.parseDouble(locSnapshot.child("latitude").getValue().toString());
                        double longitude = Double.parseDouble(locSnapshot.child("longitude").getValue().toString());
                        Location loc = new Location(latitude, longitude);
                        routes.add(loc);
                    }

                    double duration = Double.parseDouble(snapshot.child("duration").getValue().toString());
                    double distance = Double.parseDouble(snapshot.child("distance").getValue().toString());
                    double avgSpeed = Double.parseDouble(snapshot.child("avgSpeed").getValue().toString());
                    double maxSpeed = Double.parseDouble(snapshot.child("maxSpeed").getValue().toString());
                    RunEntry runEntry = new RunEntry(snapshot.getKey(), snapshot.child("date").getValue().toString(), duration, distance, avgSpeed, maxSpeed, routes);
                    runEntries.add(runEntry);

                    lvRuns = (ListView) findViewById(R.id.lvRunRuns);
                    RunAdapter runAdapter = new RunAdapter(runEntries);
                    lvRuns.setAdapter(runAdapter);
                    lvRuns.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Intent action = new Intent(RunActivity.this, RunStatsActivity.class);
                            action.putExtra("run", new Gson().toJson(runEntries.get(i)));
                            startActivity(action);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }

    @OnClick({R.id.btnRunStartARun})
    public void startARun() {
        startActivity(new Intent(this, ActiveRunActivity.class));
    }
}
