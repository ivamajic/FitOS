package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthActionCodeException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.ivamajic.fitos.Models.HRMEntry;
import hr.ferit.ivamajic.fitos.R;

public class HRMResultActivity extends AppCompatActivity {

    public static final String RESULT = "result";

    @BindView(R.id.result_tv) TextView resultTv;
    @BindView(R.id.btnHRMResultSave) Button btnSave;

    String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hrmresult);

        ButterKnife.bind(this);

        result = getIntent().getStringExtra(RESULT);
        resultTv.setText(result);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }

    @OnClick({R.id.btnHRMResultSave})
    public void saveEntry() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        else {
            DatabaseReference database = FirebaseDatabase.getInstance().getReference("HRMEntries/" + user.getUid());

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date today = new Date();
            String date = sdf.format(today);

            HRMEntry entry = new HRMEntry(null, date, Integer.parseInt(result));
            database.push().setValue(entry);
            Toast.makeText(this, "Successfully added HR Entry", Toast.LENGTH_SHORT).show();
            Intent action = new Intent(this, MainActivity.class);
            action.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(action);
            finish();
        }
    }
}
