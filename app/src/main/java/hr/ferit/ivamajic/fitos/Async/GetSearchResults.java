package hr.ferit.ivamajic.fitos.Async;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import hr.ferit.ivamajic.fitos.Activities.SearchFoodActivity;
import hr.ferit.ivamajic.fitos.Models.API.FoodSearch.FoodSearchResponse;
import hr.ferit.ivamajic.fitos.Models.API.FoodSearch.MyItem;

public class GetSearchResults extends AsyncTask<String, Double, ArrayList<MyItem>> {

    private static final String FOOD_API = "https://api.nal.usda.gov/ndb/search/?format=json&api_key=mmNrIPvpqU19VnR3CvheitEhOLRvmrlFN8KvSX8D&q=";
    private SearchFoodActivity searchFoodActivity;
    private ArrayList<MyItem> searchItems;

    public GetSearchResults(SearchFoodActivity activity) {
        searchFoodActivity = activity;
    }

    public void attach(SearchFoodActivity activity) {
        this.searchFoodActivity = activity;
    }

    public void detach() {
        this.searchFoodActivity = null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ArrayList<MyItem> doInBackground(String... params) {
        ArrayList<MyItem> searchResults = new ArrayList<>();
        if (params == null) return searchResults;

        for (int i=0; i<params.length; i++) {
            try {
                String searchTerm = params[i];
                searchResults = retrieveSearchResults(searchTerm);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        return searchResults;
    }

    private ArrayList<MyItem> retrieveSearchResults(String searchTerm) throws IOException {
        HttpsURLConnection connection = null;
        try {
            StringBuffer response = new StringBuffer();
            URL apiURL = new URL(FOOD_API + searchTerm);
            connection = (HttpsURLConnection) apiURL.openConnection();
            if (connection.getResponseCode() == 200) {
                BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                while((inputLine = input.readLine()) != null) {
                    response.append(inputLine);
                }
                FoodSearchResponse jsonResponse = new Gson().fromJson(String.valueOf(response), FoodSearchResponse.class);
                searchItems = jsonResponse.getList().getItem();

                input.close();
            }
            else {
                Log.d("fail", "fail");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return searchItems;
    }

    @Override
    protected void onPostExecute(ArrayList<MyItem> items) {
        super.onPostExecute(items);
        if (this.searchFoodActivity != null) {
            this.searchFoodActivity.displayResult(items);
        }
    }
}


