package hr.ferit.ivamajic.fitos.Models;

import android.location.LocationListener;
import android.os.Bundle;

public class SimpleLocationListener implements LocationListener {


    @Override
    public void onLocationChanged(android.location.Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override public void onProviderEnabled(String provider) {}

    @Override public void onProviderDisabled(String provider) {}
}
