package hr.ferit.ivamajic.fitos.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.ivamajic.fitos.R;

public class HRMActivity extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_BODY_SENSORS = 1;
    SensorManager mSensorManager;
    Sensor mHRSensor;
    SensorEventListener mHRListener;

    @BindView(R.id.percentage_tv) TextView percentageTv;
    @BindView(R.id.measure_btn)  Button measureBtn;
    @BindView(R.id.measuring_tv) TextView measuringTv;


    ArrayList<Float> heartRate = new ArrayList<Float>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hrm);

        if (checkSelfPermission(Manifest.permission.BODY_SENSORS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.BODY_SENSORS}, MY_PERMISSIONS_REQUEST_BODY_SENSORS);
            return;
        }



        this.mSensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
        this.mHRSensor = this.mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);


        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.mSensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
        this.mHRSensor = this.mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.mSensorManager.unregisterListener(this.mHRListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.mSensorManager.unregisterListener(this.mHRListener);
    }


    private void updateUiWithSensorInfo(SensorEvent event) {
        switch(heartRate.size()) {
            case 0:
                percentageTv.setText("0%");
                break;
            case 1:
                percentageTv.setText("10%");
                break;
            case 2:
                percentageTv.setText("20%");
                break;
            case 3:
                percentageTv.setText("30%");
                break;
            case 4:
                percentageTv.setText("40%");
                break;
            case 5:
                percentageTv.setText("50%");
                break;
            case 6:
                percentageTv.setText("60%");
                break;
            case 7:
                percentageTv.setText("70%");
                break;
            case 8:
                percentageTv.setText("80%");
                break;
            case 9:
                percentageTv.setText("90%");
                break;
            case 10:
                percentageTv.setText("100%");
                break;

        }
        if (event.values[0] > 0) {
            heartRate.add(event.values[0]);
        }
        if (heartRate.size() == 10) {
            measuringTv.setVisibility(View.GONE);
            DecimalFormat decimalFormat = new DecimalFormat("0");


            String msg = decimalFormat.format(heartRate.get(9));
            this.mSensorManager.unregisterListener(this.mHRListener);
            Intent result = new Intent(this, HRMResultActivity.class);
            result.putExtra(HRMResultActivity.RESULT, msg);
            startActivity(result);
            finish();
        }
    }

    @OnClick({R.id.measure_btn})
    public void startMeasuring() {
        if (this.mHRSensor != null && this.mHRListener != null) {
            Log.d("test", "test");
            this.mSensorManager.registerListener(this.mHRListener, this.mHRSensor, SensorManager.SENSOR_DELAY_FASTEST);
            measureBtn.setVisibility(View.GONE);
            measuringTv.setText("Measuring...");
        }

        this.mHRListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                if (event.sensor.getType() == Sensor.TYPE_HEART_RATE) {
                    updateUiWithSensorInfo(event);
                }
            }
            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {}
        };
    }


}
